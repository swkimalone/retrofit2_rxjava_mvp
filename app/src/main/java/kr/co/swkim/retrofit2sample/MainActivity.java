package kr.co.swkim.retrofit2sample;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements MainContact.View{
    MainPresenter presenter;

    @BindView(R.id.timeLabel)
    TextView mTimeLabel;
    @BindView(R.id.temperatureLabel)
    TextView mTemperatureLabel;
    @BindView(R.id.humidityValue)
    TextView mHumidityValue;
    @BindView(R.id.precipValue)
    TextView mPrecipValue;
    @BindView(R.id.summaryLabel)
    TextView mSummaryLabel;
    @BindView(R.id.iconImageView)
    ImageView mIconImageView;
    @BindView(R.id.refresh)
    ImageView mRefreshImageView;
    @BindView(R.id.progressBar)
    ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        presenter = new MainPresenter(this,this);

        hideProgress();
        mSummaryLabel.setText("Fetching current data...");
        mRefreshImageView.setOnClickListener(v -> presenter.doNetworkOpp());
    }


    //update UI
    @Override
    public void updateDisplay(CurrentWeather.currently currently) {
        mTemperatureLabel.setText(currently.getTemperature() + "");
        mHumidityValue.setText(currently.getHumidity() + "%");
        mIconImageView.setImageResource(currently.getIconID());
        mPrecipValue.setText(currently.getPercipChance() + "");
        mSummaryLabel.setText(currently.getSummary());
        mTimeLabel.setText("At " + currently.getCurrentTime() + " it will be");
    }

    @Override
    public void hideProgress() {
        mProgressBar.setVisibility(View.INVISIBLE);
        mRefreshImageView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showProgress() {
        mProgressBar.setVisibility(View.VISIBLE);
        mRefreshImageView.setVisibility(View.INVISIBLE);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        //un subscribe from all subscriptions made
        presenter.unSubscribe();
    }
}

