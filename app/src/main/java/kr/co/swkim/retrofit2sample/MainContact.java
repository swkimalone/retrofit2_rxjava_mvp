package kr.co.swkim.retrofit2sample;

/**
 * Created by swkim on 2017-01-18.
 */

public class MainContact {

    interface Presenter{
        void doNetworkOpp();
        void unSubscribe();
    }
    interface View{
        void updateDisplay(CurrentWeather.currently currently);
        void hideProgress();
        void showProgress();
    }
}
