package kr.co.swkim.retrofit2sample;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;

import kr.co.swkim.retrofit2sample.API.Service;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

import static android.content.Context.LOCATION_SERVICE;

/**
 * Created by swkim on 2017-01-18.
 */

public class MainPresenter implements MainContact.Presenter, LocationListener {

    MainContact.View mView;
    Context mContext;

    private boolean refreshing = false;
    CompositeSubscription cs;
    Observable mainObservable = null;

    boolean checkGPS = false;
    boolean checkNetwork = false;
    protected LocationManager locationManager;
    Location loc;

    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10;
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1;


    public MainPresenter(MainContact.View mView, Context context) {
        this.mView = mView;
        this.mContext = context;
        cs = new CompositeSubscription();

        locationManager = (LocationManager) mContext.getSystemService(LOCATION_SERVICE);
    }

    @Override
    public void doNetworkOpp() {
        String availProvider =availNetwork();
        if (availProvider ==null){
            return;
        }
        loc = getLocation(availProvider);

        refreshing = true;
        decideProgress();
        if (mainObservable == null) {
            mainObservable = Observable.defer(() -> {
                try {
                    return Observable.just(true)
                            .filter(aBoolean -> isNetworkAvailable()).flatMap(new Func1<Boolean, Observable<CurrentWeather.currently>>() {
                                @Override
                                public Observable<CurrentWeather.currently> call(Boolean aBoolean) {
                                    return Service.hitRetro().currentWeather(String.valueOf(loc.getLatitude()), String.valueOf(loc.getLongitude()))
                                            .flatMap(new Func1<CurrentWeather, Observable<CurrentWeather.currently>>() {
                                                @Override
                                                public Observable<CurrentWeather.currently> call(CurrentWeather currentWeather) {
                                                    return Observable.just(currentWeather.getCurrently());
                                                }
                                            });
                                }
                            });
                } catch (Exception e) {
                    return Observable.error(e);
                }
            })
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread());
        }

        cs.add(
                mainObservable.subscribe(
                        new Action1<CurrentWeather.currently>() {
                            @Override
                            public void call(CurrentWeather.currently currently) {
                                mView.updateDisplay(currently);
                            }
                        },
                        throwable -> {

                        },
                        () -> {
                            refreshing = false;
                            decideProgress();
                        }));
    }

    private String availNetwork(){
        // getting network status
        checkNetwork = locationManager
                .isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        // getting GPS status
        checkGPS = locationManager
                .isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (!checkNetwork && !checkGPS){
            Intent intent = new Intent(
                    Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            mContext.startActivity(intent);
            return null;
        }

        if (checkNetwork) {
            return LocationManager.NETWORK_PROVIDER;
        }
        else {
            return LocationManager.GPS_PROVIDER;
        }
    }

    private Location getLocation(String provider) {
        Location location = null;
        try {
            locationManager.requestLocationUpdates(
                    provider,
                    MIN_TIME_BW_UPDATES,
                    MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
            Log.d("Network", "Network");
            if (locationManager != null) {
                location = locationManager
                        .getLastKnownLocation(provider);
            }
        } catch (SecurityException e) {
            e.printStackTrace();
        }
        return location;
    }

    @Override
    public void unSubscribe() {
        cs.unsubscribe();
    }


    private boolean isNetworkAvailable() {
        ConnectivityManager manager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }

    private void decideProgress() {
        if (refreshing)
            mView.showProgress();
        else
            mView.hideProgress();
    }

    @Override
    public void onLocationChanged(Location location) {
        loc = location;
        doNetworkOpp();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
