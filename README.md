# README #

날씨정보를 [forecast](https://darksky.net/dev/)로 조회하여 보여주는 앱 

### 환경설정 ###

* Retrofit2
* Rxjava 1.1.6
* RxAndroid 1.2.1
* Retrolambda 

### PERMISSION ###

* android.permission.ACCESS_COARSE_LOCATION
* android.permission.ACCESS_FINE_LOCATION
* android.permission.INTERNET
* android.permission.ACCESS_NETWORK_STATE
